package com.dave.services.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

public class DirectoryTest {

	@Test
	public void listMatchingFiles() {
		// Looking for files
		String path = "E:\\repositories\\vmaxlive-src\\cascade-webapp";
		Directory directoryUtil = new Directory(path);
		directoryUtil.setSearchText("propert");
		directoryUtil.setOmitText("_es");
		directoryUtil.setExactlyWord(false);
		ArrayList<String> list = directoryUtil.search();

		if (list.size() <= 0) {
			System.err.println("There are no files in the path: '" + path + "'");
			return;
		}

		StringBuffer result = new StringBuffer();

		/*
		 * for (String file : list) { System.out.println(file); }
		 */

		// Find the difference between two commits
		int diffAmount = 0;
		for (String file : list) {
			Assert.assertTrue(file != null && !file.isEmpty());

			String differences = Git.getDiff("5.10.0.38", "5.11.5.3", "", file);
			if (!differences.isEmpty()) {
				diffAmount++;
				System.out.println(file);
				System.out.println("\r\ndifferences:");
				System.out.println("\r\n" + differences + "\r\n");
				result.append(file + "\r\ndifferences:\r\n" + differences + "\r\n");
			}
		}
		System.out.println("\r\n" + list.size() + " files found ");
		System.out.println(" with " + diffAmount + " differences.");
		result.append("\r\n" + list.size() + " files found with " + diffAmount + " differences.");

		// Omitted paths
		list = directoryUtil.getOmittedPaths();
		if (list.size() > 0) {
			result.append("\r\nOmitted files: ");
			System.out.println("\r\nOmitted files: ");
			for (String file : list) {
				Assert.assertTrue(file != null && !file.isEmpty());
				System.out.println(file);
				result.append("\r\n" + file);
			}
			result.append("\r\n" + list.size() + " omitted files found.");
			System.out.println("\r\n" + list.size() + " omitted files found.");
		}
		try {
			PrintWriter pw = new PrintWriter("e:/result.txt", "UTF-8");
			pw.write(result.toString());
			pw.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
