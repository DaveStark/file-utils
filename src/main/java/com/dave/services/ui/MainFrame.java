package com.dave.services.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

public class MainFrame extends JFrame implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2072971835218793491L;

	JDesktopPane desktop;
	JMenuBar menubar;
	JMenuItem gitMenuItem;
	JMenuItem fileSearchMenuItem;
	JMenuItem exitMenu;
	JMenuItem aboutMenu;
	GitUtils gitUtilsFrame;
	FileSearch fileSearch;

	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setSize(new Dimension(800, 700));
		initComponents();
	}
	
	private void initComponents(){
		desktop = new JDesktopPane();
		setLayout(new BorderLayout());
		add(desktop, BorderLayout.CENTER);
		
		menubar = new JMenuBar();
		setJMenuBar(menubar);
		
		JMenu homeMenu = new JMenu("Home");
		menubar.add(homeMenu);
		
		gitMenuItem = new JMenuItem("Git multiple comparison");
		gitMenuItem.addActionListener(this);
		homeMenu.add(gitMenuItem);
		
		fileSearchMenuItem = new JMenuItem("File search");
		fileSearchMenuItem.addActionListener(this);
		homeMenu.add(fileSearchMenuItem);
		
		homeMenu.add(new JSeparator());
		
		exitMenu = new JMenuItem("Exit");
		exitMenu.addActionListener(this);
		homeMenu.add(exitMenu);
		
		JMenu helpMenu = new JMenu("Help");
		menubar.add(helpMenu);
		
		aboutMenu = new JMenuItem("About");
		helpMenu.add(aboutMenu);
		
		gitUtilsFrame = null;
		fileSearch = null;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(gitMenuItem)){
			if(gitUtilsFrame == null)
				gitUtilsFrame = new GitUtils();
			else
				gitUtilsFrame.setVisible(true);
			desktop.add(gitUtilsFrame);
		}else if(e.getSource().equals(fileSearchMenuItem)){
			if(fileSearch == null)
				fileSearch = new FileSearch();
			else
				fileSearch.setVisible(true);
			desktop.add(fileSearch);
		}else if(e.getSource().equals(exitMenu)){
			this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
		}
	}
}
