package com.dave.services.ui;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import com.dave.services.common.Offset;

public class UiUtils {
	public static void setTextColor(JTextPane txt, Color c, int from, int to){
		StyleContext sc = StyleContext.getDefaultStyleContext();
		
		AttributeSet attrs = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);
		StyledDocument doc = txt.getStyledDocument();
		doc.setCharacterAttributes(from, to, attrs, true);
	}
	
	/***.
	 * Returns the offset of all lines which match with regex expression
	 * @param text - Text in which will be searched
	 * @param regex - 
	 * @return
	 */
	public static ArrayList<Offset> getOffset(String text, String regex){
		ArrayList<Offset> offsets = new ArrayList<Offset>();
		String[] lines = text.split(System.getProperty("line.separator"));
		int offset = 0;
		for(String line : lines){
			if(line.matches(regex)){
				offsets.add(new Offset(offset, line.length()));
			}
			offset += line.length() + 1;
		}
		return offsets;
	}
}
