package com.dave.services.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.StyledDocument;

import com.dave.services.common.Offset;
import com.dave.services.services.Directory;
import com.dave.services.services.Git;

public class GitUtils extends JInternalFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 539734668558078686L;

	JTextField txtPath;
	JTextField txtTextSearch;
	JTextField txtOmitText;
	JCheckBox chkExactlyWord;
	JTextField txtCommitFrom;
	JTextField txtCommitTo;
	JTextPane txtResult;
	JButton btnSearchAndCompare;
	JLabel lblStatus;
	String status;

	public GitUtils() {
		super("Git batch comparison", true, true, true, true);
		initComponents();
		setSize(new Dimension(700, 600));
		setPreferredSize(new Dimension(700, 600));
		pack();
		setVisible(true);
	}

	private final void initComponents() {
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.Y_AXIS));
		getContentPane().add(topPanel, BorderLayout.NORTH);

		JPanel aux = new JPanel(new FlowLayout(FlowLayout.LEFT));
		
		JLabel lblPath = new JLabel("Path  (Required)");
		aux.add(lblPath);

		txtPath = new JTextField(40);
		aux.add(txtPath);
		topPanel.add(aux);

		aux = new JPanel(new FlowLayout(FlowLayout.LEFT));
		topPanel.add(aux);
		JLabel lblTextSearch = new JLabel("Text to search (Required)");
		aux.add(lblTextSearch);

		chkExactlyWord = new JCheckBox("Exactly word");
		aux.add(chkExactlyWord);
		
		txtTextSearch = new JTextField(18);
		aux.add(txtTextSearch);
		
		aux = new JPanel(new FlowLayout(FlowLayout.LEFT));
		topPanel.add(aux);
		JLabel lblOmitText = new JLabel("Text to omit");
		aux.add(lblOmitText);

		txtOmitText = new JTextField(15);
		aux.add(txtOmitText);

		aux = new JPanel(new FlowLayout(FlowLayout.LEFT));
		topPanel.add(aux);
		JLabel lblCommitFrom = new JLabel("Commit from  (Required)");
		aux.add(lblCommitFrom);
		
		txtCommitFrom = new JTextField(15);
		aux.add(txtCommitFrom);

		JLabel lblCommitTo = new JLabel("Commit to  (Required)");
		aux.add(lblCommitTo);		

		txtCommitTo = new JTextField(15);
		aux.add(txtCommitTo);

		aux = new JPanel(new FlowLayout(FlowLayout.LEFT));
		topPanel.add(aux);
		btnSearchAndCompare = new JButton("Search and Compare");
		btnSearchAndCompare.addActionListener(this);
		aux.add(btnSearchAndCompare);	
		
		JPanel bodyPanel = new JPanel(new BorderLayout());
		getContentPane().add(bodyPanel, BorderLayout.CENTER);
		
		JLabel lblResult = new JLabel("Results");
		bodyPanel.add(lblResult, BorderLayout.NORTH);

		txtResult = new JTextPane();
		JScrollPane scrollTxtResultPane = new JScrollPane(txtResult);
		bodyPanel.add(scrollTxtResultPane, BorderLayout.CENTER);
		
		lblStatus = new JLabel("");
		bodyPanel.add(lblStatus,BorderLayout.SOUTH);
	}

	private boolean areFieldsEmpty() {
		boolean result = true;
		result = txtPath.getText().isEmpty() || txtTextSearch.getText().isEmpty() || txtCommitFrom.getText().isEmpty()
				|| txtCommitTo.getText().isEmpty();
		return result;
	}

	private void performSearchAndComparison(){
		Thread thread = new Thread() {

			@Override
			public void run() {
				btnSearchAndCompare.setEnabled(false);
				lblStatus.setText("Starting...");
				searchAndComparison();
				btnSearchAndCompare.setEnabled(true);
			}
		};
		thread.start();
	}
	
	private void searchAndComparison() {
		txtResult.setText("");
		StyledDocument doc = txtResult.getStyledDocument();
		try {
			Directory directoryUtil = new Directory(txtPath.getText());
			directoryUtil.setSearchText(txtTextSearch.getText());
			directoryUtil.setOmitText(txtOmitText.getText());
			directoryUtil.setExactlyWord(chkExactlyWord.isSelected());
			doc.insertString(doc.getLength(), "Looking for files...\r\n", null);
			txtResult.revalidate();
			lblStatus.setText("Searching files...");
			lblStatus.revalidate();
			ArrayList<String> list = directoryUtil.search();

			if (list.size() > 0) {
				doc.insertString(doc.getLength(), list.size() + " files found.\r\n", null);
				doc.insertString(doc.getLength(), directoryUtil.getOmittedPaths().size() + " files omitted.\r\n", null);
				doc.insertString(doc.getLength(), "Finding files with differences...", null);
				txtResult.revalidate();
				int differencesAmount = 0;
				for (String file : list) {
					differencesAmount++;
					lblStatus.setText("Still there are " + String.valueOf(list.size() - differencesAmount) +  " files of " + list.size() 
						+ " files, looking for differences in '" + file + "' file...");
					String differences = Git.getDiff(txtCommitFrom.getText(), txtCommitTo.getText(), "", file);
					if (!differences.isEmpty()) {
						doc.insertString(doc.getLength(), "\r\ndifferences:\r\n", null);
						doc.insertString(doc.getLength(), differences + "\r\n", null);
					}
					txtResult.setCaretPosition(txtResult.getDocument().getLength());
				}
				setGitTextStyle(txtResult);
				doc.insertString(doc.getLength(),differencesAmount + " files with differences found.\r\n", null);
			} else {
				doc.insertString(doc.getLength(), "There are no files in the path: '" + txtPath.getText() + "'", null);
			}
			doc.insertString(doc.getLength(),"Process has end successfully.\r\n", null);
			txtResult.revalidate();
		} catch (Exception ex) {
			txtResult.setText(ex.getMessage());
		}
		lblStatus.setText("Search and comparison has finished.");
	}
	
	private void setGitTextStyle(JTextPane txt){
		//"^\+.*" <- Means = Start with + symbol and after that can be any character
		ArrayList<Offset> addedLinesMatches = UiUtils.getOffset(txt.getText(), "^\\+.*");
		ArrayList<Offset> removedLinesMatches = UiUtils.getOffset(txt.getText(), "^\\-.*");
		for(Offset offset: addedLinesMatches){
			UiUtils.setTextColor(txt, Color.GREEN, offset.getStart(), offset.getEnd());
		}
		
		for(Offset offset: removedLinesMatches){
			UiUtils.setTextColor(txt, Color.RED, offset.getStart(), offset.getEnd());
		}
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(btnSearchAndCompare)) {
			if (!areFieldsEmpty()) {
				performSearchAndComparison();
			} else {
				JOptionPane.showMessageDialog(this, "Please fill require fields", "Empty fields",
						JOptionPane.WARNING_MESSAGE);
			}
		}
	}

}
