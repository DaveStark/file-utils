package com.dave.services.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.dave.services.domain.FileContent;
import com.dave.services.services.Directory;

public class FileSearch extends JInternalFrame implements ActionListener, ListSelectionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1376488121208500041L;

	JTextField txtPath;
	JTextField txtTextSearch;
	JTextPane txtFileContent;
	JList<String> lstFoundFiles;
	String currentFoundFileSelected;
	JButton btnSearch;
	ArrayList<FileContent> foundList;
	JLabel lblStatus;
	boolean isSearching;
	Directory directory;

	public FileSearch() {
		super("Search files", true, true, true, true);
		initComponents();
		setSize(new Dimension(600, 600));
		setPreferredSize(new Dimension(600, 600));
		pack();
		setVisible(true);
		currentFoundFileSelected = "";
		isSearching = false;
		directory = null;
	}

	private final void initComponents() {
		setLayout(new BorderLayout());
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.Y_AXIS));
		getContentPane().add(topPanel, BorderLayout.NORTH);

		JPanel topUpPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		topPanel.add(topUpPanel);

		JLabel lblPath = new JLabel("Path");
		topUpPanel.add(lblPath);

		txtPath = new JTextField(40);
		topUpPanel.add(txtPath);

		JPanel topDownPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		topPanel.add(topDownPanel);

		JLabel lblTextSearch = new JLabel("Text search");
		topDownPanel.add(lblTextSearch);

		txtTextSearch = new JTextField(20);
		topDownPanel.add(txtTextSearch);

		btnSearch = new JButton("Search");
		btnSearch.addActionListener(this);
		topDownPanel.add(btnSearch);

		lstFoundFiles = new JList<>();
		lstFoundFiles.addListSelectionListener(this);
		JPanel bodyLeftPanel = new JPanel(new BorderLayout());
		bodyLeftPanel.add(lstFoundFiles);

		txtFileContent = new JTextPane();
		txtFileContent.setEditable(false);
		JScrollPane bodyRightPanel = new JScrollPane(txtFileContent);

		JSplitPane splitPaneBody = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, bodyLeftPanel, bodyRightPanel);
		// splitPaneBody.setDividerLocation(170);
		splitPaneBody.setResizeWeight(0.5);
		getContentPane().add(splitPaneBody, BorderLayout.CENTER);
		bodyLeftPanel.setMinimumSize(new Dimension(50, 50));
		bodyRightPanel.setMinimumSize(new Dimension(50, 50));
		
		lblStatus = new JLabel("");
		getContentPane().add(lblStatus, BorderLayout.SOUTH);
	}

	/**
	 * Validates if whole fields are empty or not
	 * 
	 * @return
	 */
	private boolean areFieldsEmpty() {
		boolean result = true;
		result = txtPath.getText().isEmpty() || txtTextSearch.getText().isEmpty();
		return result;
	}

	/**
	 * Performs the search in the specified directory according to text search
	 */
	private void performSearch() {
		lstFoundFiles.clearSelection();
		lstFoundFiles.setListData(new String[0]);		
		foundList = null;
		isSearching = true;	
		Thread thread = new Thread() {

			@Override
			public void run() {
				try{
					btnSearch.setEnabled(false);
					lblStatus.setText("Searching...");
					directory = new Directory(txtPath.getText());
					directory.setSearchText(txtTextSearch.getText());
					foundList = directory.searchInto(false);
					String[] fileList = getStringList();
					lstFoundFiles.setListData(fileList);
					isSearching = false;
					btnSearch.setEnabled(true);
					lblStatus.setText("Searching has finished.");
				}catch(Exception ex){
					isSearching = false;	
				}
			}
		};
		thread.start();
		buildSearchMonitor();
	}
	
	/**
	 * Build the monitor which will monitor the search thread
	 */
	private void buildSearchMonitor(){
		Thread thread = new Thread() {
			
			@Override
			public void run() {
				System.out.println("monitor..");
				while(isSearching){
					if(directory != null){
						lblStatus.setText("Searching in '"+ directory.getCurrentFile() + "' file...");
						lblStatus.revalidate();
					}
				}
			}
		};
		thread.start();
	}

	private String[] getStringList() {
		String[] fileList = new String[foundList.size()];
		int i = 0;
		for (FileContent fileContent : foundList) {
			fileList[i++] = fileContent.getFileName();
		}
		return fileList;
	}

	private FileContent getFileContentByName(String fileName) {
		for (FileContent fileContent : foundList) {
			if (fileContent.getFileName().equals(fileName)) {
				return fileContent;
			}
		}
		return null;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(btnSearch)) {
			if (!areFieldsEmpty()) {
				performSearch();
			} else
				JOptionPane.showMessageDialog(this, "Please fill require fields", "Empty fields",
						JOptionPane.WARNING_MESSAGE);
		}
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (e.getSource().equals(lstFoundFiles) && !currentFoundFileSelected.equals(lstFoundFiles.getSelectedValue())) {
			System.out.println("valueChanged: " + lstFoundFiles.getSelectedValue());
			txtFileContent.setText("Loading...");
			Thread thread = new Thread(){
				@Override
				public void run() {
					FileContent fileContent = getFileContentByName(lstFoundFiles.getSelectedValue());
					if (fileContent != null) {
						ArrayList<String> fullContent = fileContent.getFoundFileContent();
						String textContent = "";
						for (String content : fullContent) {
							textContent += content + "\n\n ---------------------------------------------------------\n\n";
						}
						txtFileContent.setText(textContent);
					}
				}
			};
			thread.start();
			currentFoundFileSelected = lstFoundFiles.getSelectedValue();
		}else{
			currentFoundFileSelected = "";
		}
	}
}
