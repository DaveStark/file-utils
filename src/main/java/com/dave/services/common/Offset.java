package com.dave.services.common;

public class Offset {
	private int start;
	private int end;
	
	public Offset(){
		this(0,0);
	}
	
	public Offset(int start, int  end){
		this.start = start;
		this.end = end;
	}
	
	/**
	 * @return the start
	 */
	public int getStart() {
		return start;
	}
	/**
	 * @param start the start to set
	 */
	public void setStart(int start) {
		this.start = start;
	}
	/**
	 * @return the end
	 */
	public int getEnd() {
		return end;
	}
	/**
	 * @param end the end to set
	 */
	public void setEnd(int end) {
		this.end = end;
	}
	
}
