package com.dave.services.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import com.dave.services.domain.FileContent;

public class Directory {

	private ArrayList<String> filePaths;
	private ArrayList<String> omittedfilePaths;
	private ArrayList<FileContent> fileContentList;
	private String initialPath;
	private String searchText;
	private boolean exactlyWord;
	private String omitText;
	private String currentFile;
	private boolean lookUsingfileName;

	public Directory(String initialPath) {
		this.initialPath = initialPath;
		this.filePaths = new ArrayList<>();
		this.omittedfilePaths = new ArrayList<>();
		this.fileContentList = new ArrayList<>();
		exactlyWord = false;
		omitText = searchText = "";
		setCurrentFile("");
	}

	/**
	 * Starting the search
	 * 
	 * @return
	 */
	public ArrayList<String> search() {
		if (searchText.isEmpty())
			throw new RuntimeException("'SearchText' attribute is missing.");
		searchForFile(initialPath);
		return filePaths;
	}

	/**
	 * Starting the search
	 * 
	 * @return
	 */
	public ArrayList<FileContent> searchInto(boolean lookUsingfileName) {
		if (searchText.isEmpty())
			throw new RuntimeException("'SearchText' attribute is missing.");
		this.lookUsingfileName = lookUsingfileName;
		searchIntoWholeFiles(initialPath);
		return fileContentList;
	}

	/**
	 * Search recursively for file list within initial directory specified
	 * according to search text
	 * 
	 * @param currentPath
	 */
	private void searchForFile(String currentPath) {
		File file = new File(currentPath);
		File[] elements = file.listFiles();
		for (File element : elements) {
			if (element.isFile()) {
				if (isExactlyWord() && element.getName().toLowerCase().equals(getSearchText())) {
					if (omitText.isEmpty() || !element.getName().toLowerCase().contains(omitText))
						filePaths.add(element.getAbsolutePath());
					else
						omittedfilePaths.add(element.getAbsolutePath());
				} else if (!isExactlyWord() && element.getName().toLowerCase().contains(getSearchText())) {
					if (omitText.isEmpty() || !element.getName().toLowerCase().contains(omitText))
						filePaths.add(element.getAbsolutePath());
					else
						omittedfilePaths.add(element.getAbsolutePath());
				}
			} else if (element.isDirectory()) {
				searchForFile(element.getAbsolutePath());
			}
		}
	}

	/**
	 * Search recursively within file content on specified directory according
	 * to search text
	 * 
	 * @param currentPath
	 */
	private void searchIntoWholeFiles(String currentPath) {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		File file = new File(currentPath);
		File[] elements = file.listFiles();
		for (File element : elements) {
			if (element.isFile()) {
				setCurrentFile(element.getName());
				if (lookUsingfileName && element.getName().toLowerCase().contains(getSearchText())) {
					FileContent fileContent = new FileContent();
					fileContent.setFileName(element.getName());
					fileContentList.add(fileContent);
				} else {					
					try {
						ArrayList<String> content = searchIntoFileContent(element.getAbsolutePath(), getSearchText());
						if(content.size()>0){
							FileContent fileContent = new FileContent();
							fileContent.setFileName(element.getName());
							fileContent.setFileContentFound(content);
							fileContentList.add(fileContent);
						}
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			} else if (element.isDirectory()) {
				searchIntoWholeFiles(element.getAbsolutePath());
			}
		}
	}

	private ArrayList<String> searchIntoFileContent(String filePath, String searchText) throws IOException {
		File file = new File(filePath);
		ArrayList<String> fileContents = new ArrayList<String>();
		
		if (!file.isFile())
			throw new FileNotFoundException("The file " + filePath + " was not found.");
		
		String line = "";
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
		int lineNumber = 0;
		if(br.ready()){
			while ((line = br.readLine()) != null) {
				lineNumber++;
				if (line.contains(searchText)) {
					fileContents.add(line);
				}
			}
		}else{
			System.out.println("This file '" + filePath + "' couldn't be read.");
		}
		br.close();
		return fileContents;
	}

	/**
	 * @return the searchText
	 */
	public String getSearchText() {
		return searchText;
	}

	/**
	 * @param searchText
	 *            Portion or full name of the file that will be searched
	 */
	public void setSearchText(String searchText) {
		this.searchText = searchText.toLowerCase();
	}

	/**
	 * @return the exactlyWord
	 */
	public boolean isExactlyWord() {
		return exactlyWord;
	}

	/**
	 * @param exactlyWord
	 *            Indicates if the search will be exactly the word or just
	 *            contained it
	 */
	public void setExactlyWord(boolean exactlyWord) {
		this.exactlyWord = exactlyWord;
	}

	/**
	 * @return the omitText
	 */
	public String getOmitText() {
		return omitText;
	}

	/**
	 * @param omitText
	 *            the omitText to set
	 */
	public void setOmitText(String omitText) {
		this.omitText = omitText.toLowerCase();
	}

	/**
	 * Returns omitted match files
	 * 
	 * @return omitted match files
	 */
	public ArrayList<String> getOmittedPaths() {
		return omittedfilePaths;
	}

	/**
	 * Returns the parent directory if path parameter is a file, otherwise
	 * returns the same specified directory or empty if it's not any of both
	 * 
	 * @param path
	 * @return
	 */
	public static String getDirectory(String path) {
		File file = new File(path);
		if (file.isDirectory())
			return file.getAbsolutePath();
		else if (file.isFile()) {
			return file.getParentFile().getAbsolutePath();
		}
		return "";
	}

	/**
	 * @return the currentFile
	 */
	public String getCurrentFile() {
		return currentFile;
	}

	/**
	 * @param currentFile the currentFile to set
	 */
	private void setCurrentFile(String currentFile) {
		this.currentFile = currentFile;
	}
}
