package com.dave.services.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ShellCommand {

	public static String executeWindowsCommand(String command, String path) {
		StringBuffer output = new StringBuffer();
		Process p;
		try {
			command =  "cd /d \"" + path + "\" && " + command; 
			//p = Runtime.getRuntime().exec(command);
			ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/C", command);
			p = builder.start();
			//p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream(), "UTF-8"));
			String line = "";
			while (true) {
				line = reader.readLine();
				if(line == null) break;
				output.append(line + "\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output.toString();
	}

}
