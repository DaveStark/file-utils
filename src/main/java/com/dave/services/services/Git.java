package com.dave.services.services;

public class Git {

	public Git() {

	}

	public static String getDiff(String firstCommit, String secondCommit, String options, String path) {
		String cmd = "git diff";
		cmd += " " + firstCommit + " " + secondCommit + " " + options + " " + path ;
		String result = ShellCommand.executeWindowsCommand(cmd, Directory.getDirectory(path));
		return result;
	}

	
}
