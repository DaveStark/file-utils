package com.dave.services.domain;

import java.util.ArrayList;

public class FileContent {
	private String fileName;
	private ArrayList<String> fileContentFound;
	
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @return the fileContentFound
	 */
	public ArrayList<String> getFoundFileContent() {
		return fileContentFound;
	}
	/**
	 * @param fileContentFound the fileContentFound to set
	 */
	public void setFileContentFound(ArrayList<String> fileContentFound) {
		this.fileContentFound = fileContentFound;
	}
}
